package com.javagda19.designpatterns.construct.singleton_zad1;

import java.util.Scanner;

public class MainZad1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        PlacowkaPoczty placowkaPoczty = new PlacowkaPoczty();

        String komenda;
        do {
            komenda = scanner.nextLine();

            int numerek = -1;
            if (komenda.startsWith("a")) {  // automat
                // wywołaj metodę pobrania z automatu
                numerek = placowkaPoczty.pobierzNumerekZAutomatu();
            } else if (komenda.startsWith("r")) { //
                // wywołaj pobranie z rejestracji
                numerek = placowkaPoczty.pobierzNumerekZRejestracji();
            } else if (komenda.startsWith("s")) {
                // wywołaj pobranie z serwisu internetowego
                numerek = placowkaPoczty.pobierzNumerekZSerwisuInternetowego();
            }

            if(numerek != -1) {
                System.out.println("Numerek: " + numerek);
            }

        } while (!komenda.equalsIgnoreCase("quit"));
    }
}


package com.javagda19.designpatterns.construct.singleton_zad1;

public class GeneratorNumerkow {
    private static GeneratorNumerkow ourInstance = new GeneratorNumerkow();

    public static GeneratorNumerkow getInstance() {
        return ourInstance;
    }

    private GeneratorNumerkow() {
    }

    private int licznik = 1;

    public synchronized int getLicznik() {
        return licznik++; // post inkrementacja
    }
}

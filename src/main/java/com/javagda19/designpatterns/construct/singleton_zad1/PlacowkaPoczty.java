package com.javagda19.designpatterns.construct.singleton_zad1;

public class PlacowkaPoczty {
    private Rejestracja rejestracja = new Rejestracja();
    private SerwisInternetowy serwisInternetowy = new SerwisInternetowy();
    private Automat automat = new Automat();

    public int pobierzNumerekZRejestracji() {
        return rejestracja.pobierzNumerek();
    }

    public int pobierzNumerekZSerwisuInternetowego() {
        return serwisInternetowy.pobierzNumerek();
    }

    public int pobierzNumerekZAutomatu() {
        return automat.pobierzNumerek();
    }
}

package com.javagda19.designpatterns.construct.singleton_zad1;

public enum GeneratorEnum {
    INSTANCE;

    private int licznik = 1;

    public synchronized int getLicznik() {
        return licznik++; // post inkrementacja
    }
}

package com.javagda19.designpatterns.construct.zad1_factory_method;

public enum ComputerBrand {
    ASUS, APPLE, HP, SAMSUNG
}

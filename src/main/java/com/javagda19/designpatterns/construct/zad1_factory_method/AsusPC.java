package com.javagda19.designpatterns.construct.zad1_factory_method;

public class AsusPC extends AbstractPC {
    private AsusPC(String name, int power, double gpuPower, boolean overclocked) {
        super(name, ComputerBrand.ASUS, power, gpuPower, overclocked);
    }

    public static AsusPC createAsusN53() {
        return new AsusPC("N53", 93, 28, false);
    }

    public static AsusPC createSuperHiperMegaUltraProEnterpriseLaptop() {
        return new AsusPC("HSHMUL", 100, 1.0, true);
    }
}

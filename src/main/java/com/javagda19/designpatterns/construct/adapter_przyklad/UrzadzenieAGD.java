package com.javagda19.designpatterns.construct.adapter_przyklad;

public interface UrzadzenieAGD {
    public void wlacz();
    public void wylacz();
}

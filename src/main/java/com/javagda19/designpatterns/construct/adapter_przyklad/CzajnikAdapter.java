package com.javagda19.designpatterns.construct.adapter_przyklad;

public class CzajnikAdapter /*extends Czajnik*/ implements UrzadzenieAGD {
    private Czajnik czajnik;

    public CzajnikAdapter(Czajnik czajnik) {
        this.czajnik = czajnik;
    }

    @Override
    public void wlacz() {
        czajnik.turnOn();
//        turnOn();
    }

    @Override
    public void wylacz() {
        czajnik.turnOff();
//        turnOff();
    }
}

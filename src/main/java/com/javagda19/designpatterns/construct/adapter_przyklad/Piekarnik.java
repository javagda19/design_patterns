package com.javagda19.designpatterns.construct.adapter_przyklad;

public class Piekarnik {
    public void enable(){
        System.out.println("Enabled");
    }

    public void disable(){
        System.out.println("Disable");
    }
}

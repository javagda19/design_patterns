package com.javagda19.designpatterns.construct.adapter_przyklad;

public final class Czajnik {

    public void turnOn(){
        System.out.println("Turning on czajnik");
    }

    public void turnOff(){
        System.out.println("Turning off");
    }
}

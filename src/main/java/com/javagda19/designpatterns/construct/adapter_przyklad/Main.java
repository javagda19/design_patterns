package com.javagda19.designpatterns.construct.adapter_przyklad;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Czajnik czajnik = new Czajnik();
        Lodówka lodówka= new Lodówka();
        Piekarnik piekarnik = new Piekarnik();

        UrzadzenieAGD[] urzadzenieAGDS = new UrzadzenieAGD[3];
        urzadzenieAGDS[0] = new CzajnikAdapter(czajnik);
        urzadzenieAGDS[1] = new LodówkaAdapter(lodówka);

    }
}

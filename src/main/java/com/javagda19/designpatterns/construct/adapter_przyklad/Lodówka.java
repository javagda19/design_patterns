package com.javagda19.designpatterns.construct.adapter_przyklad;

public class Lodówka {
    public void switchOn(){
        System.out.println("Włączam lodówke");
    }

    public void switchOff(){
        System.out.println("Wyłączam lodówke");
    }
}

package com.javagda19.designpatterns.construct.adapter_przyklad;

public class LodówkaAdapter implements UrzadzenieAGD{
    private Lodówka lodówka;

    public LodówkaAdapter(Lodówka lodówka) {
        this.lodówka = lodówka;
    }

    @Override
    public void wlacz() {
        lodówka.switchOn();
    }

    @Override
    public void wylacz() {
        lodówka.switchOff();
    }
}

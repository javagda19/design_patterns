package com.javagda19.designpatterns.construct.builder_example;

public class Bohater {
    private int iloscZycia, iloscMany, iloscPunktowAtaku, iloscPunktowObrony, numerButa, szerokoscWPasie, obwodGlowy, wysokosc, waga;
    private String imie, nazwisko, drugieImie, trzecieImie, imieMatki, imieOjca, imieSiostry, imieDrugiejSiostry, imieBabci, imieDziadka, imieDrugiejBabci, imieDrugiegoDziadka, pesel;

    /**
     * pkt
     **/
    public double iloscPunktow;

    private Bohater(int iloscZycia, int iloscMany, int iloscPunktowAtaku, int iloscPunktowObrony, int numerButa, int szerokoscWPasie, int obwodGlowy, int wysokosc, int waga, String imie, String nazwisko, String drugieImie, String trzecieImie, String imieMatki, String imieOjca, String imieSiostry, String imieDrugiejSiostry, String imieBabci, String imieDziadka) {
        this.iloscZycia = iloscZycia;
        this.iloscMany = iloscMany;
        this.iloscPunktowAtaku = iloscPunktowAtaku;
        this.iloscPunktowObrony = iloscPunktowObrony;
        this.numerButa = numerButa;
        this.szerokoscWPasie = szerokoscWPasie;
        this.obwodGlowy = obwodGlowy;
        this.wysokosc = wysokosc;
        this.waga = waga;
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.drugieImie = drugieImie;
        this.trzecieImie = trzecieImie;
        this.imieMatki = imieMatki;
        this.imieOjca = imieOjca;
        this.imieSiostry = imieSiostry;
        this.imieDrugiejSiostry = imieDrugiejSiostry;
        this.imieBabci = imieBabci;
        this.imieDziadka = imieDziadka;
    }

    @Override
    public String toString() {
        return "Bohater{" +
                "iloscZycia=" + iloscZycia +
                ", iloscMany=" + iloscMany +
                ", iloscPunktowAtaku=" + iloscPunktowAtaku +
                ", iloscPunktowObrony=" + iloscPunktowObrony +
                ", numerButa=" + numerButa +
                ", szerokoscWPasie=" + szerokoscWPasie +
                ", obwodGlowy=" + obwodGlowy +
                ", wysokosc=" + wysokosc +
                ", waga=" + waga +
                ", imie='" + imie + '\'' +
                ", nazwisko='" + nazwisko + '\'' +
                ", drugieImie='" + drugieImie + '\'' +
                ", trzecieImie='" + trzecieImie + '\'' +
                ", imieMatki='" + imieMatki + '\'' +
                ", imieOjca='" + imieOjca + '\'' +
                ", imieSiostry='" + imieSiostry + '\'' +
                ", imieDrugiejSiostry='" + imieDrugiejSiostry + '\'' +
                ", imieBabci='" + imieBabci + '\'' +
                ", imieDziadka='" + imieDziadka + '\'' +
                ", imieDrugiejBabci='" + imieDrugiejBabci + '\'' +
                ", imieDrugiegoDziadka='" + imieDrugiegoDziadka + '\'' +
                ", pesel='" + pesel + '\'' +
                ", iloscPunktow=" + iloscPunktow +
                '}';
    }

    public static class Builder {

        private int iloscZycia;
        private int iloscMany;
        private int iloscPunktowAtaku;
        private int iloscPunktowObrony;
        private int numerButa;
        private int szerokoscWPasie;
        private int obwodGlowy;
        private int wysokosc;
        private int waga;
        private String imie;
        private String nazwisko;
        private String drugieImie;
        private String trzecieImie;
        private String imieMatki;
        private String imieOjca;
        private String imieSiostry;
        private String imieDrugiejSiostry;
        private String imieBabci;
        private String imieDziadka;

        public Builder setIloscZycia(int iloscZycia) {
            this.iloscZycia = iloscZycia;
            return this;
        }

        public Builder setIloscMany(int iloscMany) {
            this.iloscMany = iloscMany;
            return this;
        }

        public Builder setIloscPunktowAtaku(int iloscPunktowAtaku) {
            this.iloscPunktowAtaku = iloscPunktowAtaku;
            return this;
        }

        public Builder setIloscPunktowObrony(int iloscPunktowObrony) {
            this.iloscPunktowObrony = iloscPunktowObrony;
            return this;
        }

        public Builder setNumerButa(int numerButa) {
            this.numerButa = numerButa;
            return this;
        }

        public Builder setSzerokoscWPasie(int szerokoscWPasie) {
            this.szerokoscWPasie = szerokoscWPasie;
            return this;
        }

        public Builder setObwodGlowy(int obwodGlowy) {
            this.obwodGlowy = obwodGlowy;
            return this;
        }

        public Builder setWysokosc(int wysokosc) {
            this.wysokosc = wysokosc;
            return this;
        }

        public Builder setWaga(int waga) {
            this.waga = waga;
            return this;
        }

        public Builder setImie(String imie) {
            this.imie = imie;
            return this;
        }

        public Builder setNazwisko(String nazwisko) {
            this.nazwisko = nazwisko;
            return this;
        }

        public Builder setDrugieImie(String drugieImie) {
            this.drugieImie = drugieImie;
            return this;
        }

        public Builder setTrzecieImie(String trzecieImie) {
            this.trzecieImie = trzecieImie;
            return this;
        }

        public Builder setImieMatki(String imieMatki) {
            this.imieMatki = imieMatki;
            return this;
        }

        public Builder setImieOjca(String imieOjca) {
            this.imieOjca = imieOjca;
            return this;
        }

        public Builder setImieSiostry(String imieSiostry) {
            this.imieSiostry = imieSiostry;
            return this;
        }

        public Builder setImieDrugiejSiostry(String imieDrugiejSiostry) {
            this.imieDrugiejSiostry = imieDrugiejSiostry;
            return this;
        }

        public Builder setImieBabci(String imieBabci) {
            this.imieBabci = imieBabci;
            return this;
        }

        public Builder setImieDziadka(String imieDziadka) {
            this.imieDziadka = imieDziadka;
            return this;
        }

        public Bohater createBohater() {
            return new Bohater(iloscZycia, iloscMany, iloscPunktowAtaku, iloscPunktowObrony, numerButa, szerokoscWPasie, obwodGlowy, wysokosc, waga, imie, nazwisko, drugieImie, trzecieImie, imieMatki, imieOjca, imieSiostry, imieDrugiejSiostry, imieBabci, imieDziadka);
        }
    }
}

package com.javagda19.designpatterns.construct.builder_example;

public class Main {
    public static void main(String[] args) {
//        Bohater bohater = new Bohater(0,0,0,0,0,0,0,0,0,"Janek", "Kowalski", null, null

        Bohater bohater =
                new Bohater.Builder()//.setIloscZycia(100)
                .setIloscMany(50)
                .setImie("Janek")
                .setNazwisko("Kowalski")
                .setIloscMany(100)
                .setImieMatki("Janka").createBohater();

//        bohater.
        System.out.println(bohater);
    }
}

package com.javagda19.designpatterns.construct.facade_przyklad;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
        Rocket rocket = new Rocket();

//        startRocket(rocket);

    }

    public static void startRocket(IRocket rocket){
        rocket.enableAutopilotToMars();
    }
}

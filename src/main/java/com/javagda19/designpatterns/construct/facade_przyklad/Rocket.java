package com.javagda19.designpatterns.construct.facade_przyklad;

public class Rocket implements IRocketDebugger {
    private int height;
    private int speed;

    public void startEngines() {
        System.out.println("Starting engines");
    }

    public void stopEngines() {
        System.out.println("Stop engines");
    }

    public void ignition() {
        System.out.println("Ignition");
    }

    public void disconnectFuelTanks() {
        System.out.println("FuelTanks Disconnected");
    }

    public int getSpeed() {
        return speed;
    }

    public int getHeight() {
        return height;
    }

    protected void selfDestruct(){
        System.out.println("Self destructing in 10 s");
    }

    public void ejectPilot(){
        System.out.println("Ejecting pilots");
    }

    @Override
    public void checkFuelTanks() {

    }

    @Override
    public void checkEjectingModule() {

    }

    @Override
    public void checkEngine() {

    }
}

package com.javagda19.designpatterns.construct.facade_przyklad;

public class RocketFacade implements IRocketDebugger, IRocket {
    private Rocket rocket;

    public RocketFacade(Rocket rocket) {
        this.rocket = rocket;
    }

    @Override
    public void enableAutopilotToMars() {
        rocket.startEngines();
        rocket.ignition();
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        rocket.disconnectFuelTanks();

        System.out.println("Height is:" + rocket.getHeight());
    }

    @Override
    public void checkFuelTanks() {

    }

    @Override
    public void checkEjectingModule() {

    }

    @Override
    public void checkEngine() {

    }
}

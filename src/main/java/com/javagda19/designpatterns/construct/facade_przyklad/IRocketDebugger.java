package com.javagda19.designpatterns.construct.facade_przyklad;

public interface IRocketDebugger {
    void checkFuelTanks();
    void checkEjectingModule();
    void checkEngine();

}

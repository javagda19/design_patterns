package com.javagda19.designpatterns.construct.facade_przyklad;

public class Mechanic {
    public void doCheckupWithRocket(IRocketDebugger rocket) {
        rocket.checkEjectingModule();
        rocket.checkEngine();
        rocket.checkFuelTanks();
    }
}

package com.javagda19.designpatterns.construct.facade_przyklad;

public interface IRocket {
    public void enableAutopilotToMars();
}

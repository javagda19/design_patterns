package com.javagda19.designpatterns.construct.singleton_example;

public class ConfigFileManager {
    private static ConfigFileManager INSTANCE;// =
//            new ConfigFileManager(); //eager / lazy

    private ConfigFileManager() {
    }

    public static synchronized ConfigFileManager getInstance() {
        // 1 2
        if (INSTANCE == null) {
            // 1 2
            synchronized (ConfigFileManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ConfigFileManager(); // lazy
                }
            }
        }
        return INSTANCE;
    }

    public synchronized void operacjaNaPliku() {
//        synchronized (lock) {
//
//        }
    }
}

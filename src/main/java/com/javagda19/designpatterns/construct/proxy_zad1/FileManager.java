package com.javagda19.designpatterns.construct.proxy_zad1;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileManager implements ISimpleFileManager{
    private BufferedReader bufferedReader = null;
    private String sciezkaDoPliku;

    public FileManager(String sciezkaDoPliku) {
        this.sciezkaDoPliku = sciezkaDoPliku;
    }

    public void otworzPlik() {
        try {
            bufferedReader = new BufferedReader(new FileReader(sciezkaDoPliku));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void zamknijPlik() {
        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        bufferedReader = null;
    }

    public String wczytajLinie() throws IOException {
        if (bufferedReader == null) {
            otworzPlik();
        }
        String linia;

        linia = bufferedReader.readLine();
        if (linia == null) { // plik się skończył
            przewinPlik();
            return wczytajLinie();
        }
        return linia;
    }

    public void przewinPlik() {
        zamknijPlik();
        otworzPlik();
    }

    public void pominLinie() {
        try {
            wczytajLinie();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void pominNlinii(int n) {
        for (int i = 0; i < n; i++) {
            pominLinie();
        }
    }

    public List<String> wczytajNLinii(int n) throws IOException {
        List<String> list = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            list.add(wczytajLinie());
        }
        return list;
    }
}

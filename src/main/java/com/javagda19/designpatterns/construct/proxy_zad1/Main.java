package com.javagda19.designpatterns.construct.proxy_zad1;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        ISimpleFileManager fileManager = new FileManager("pom.xml");
        try {
            if(fileManager instanceof FileManager){
                FileManager fm = (FileManager) fileManager;
                fm.zamknijPlik();
            }
            fileManager.wczytajNLinii(67).stream().forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void x(ISimpleFileManager is){

    }
}

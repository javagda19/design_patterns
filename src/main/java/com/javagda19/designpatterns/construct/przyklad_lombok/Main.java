package com.javagda19.designpatterns.construct.przyklad_lombok;

import com.sun.istack.internal.logging.Logger;
import lombok.extern.java.Log;

import java.util.HashSet;
import java.util.Set;


@Log
public class Main {
    public static void main(String[] args) {
        Student s = new Student("awdawd", "awdawd", "a", null);

        Ocena o = new Ocena(5.0, s);
        s.setOcena(o);

        Student x = new Student.StudentBuilder().poleExcluded("a").build();
    }
}

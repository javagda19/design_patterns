package com.javagda19.designpatterns.construct.przyklad_lombok;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ocena {
    private double wartosc;
    private Student student;
}

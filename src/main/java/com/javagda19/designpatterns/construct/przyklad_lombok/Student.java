package com.javagda19.designpatterns.construct.przyklad_lombok;

import lombok.*;
import lombok.extern.java.Log;


@Data // @RequiredArgsConstructor
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    private String imiessefhseugyfjgysebhes;
    private String costam;

    @ToString.Exclude
    private String poleExcluded;

    private Ocena ocena;



}

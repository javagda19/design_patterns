package com.javagda19.designpatterns.construct.observerObservable;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NewsMessage {
    private String tresc;
    private int waga;
}

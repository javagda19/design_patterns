package com.javagda19.designpatterns.construct.observerObservable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        List<String> xyz = new LinkedList<>();
//        xyz.add("abc");
//        xyz.add("def");
//        xyz.add("wac");
//        for (String x : xyz) {
//            if(x.equalsIgnoreCase("abc")) {
//                xyz.remove(x);
//            }
//        }

        NewsStation newsStation = new NewsStation("RVN");

        newsStation.dodajOgladajacego(new Watcher(3, "Olek"));
        newsStation.dodajOgladajacego(new Watcher(5, "Arek"));
        newsStation.dodajOgladajacego(new Watcher(7, "Kuba"));
        newsStation.dodajOgladajacego(new Watcher(10, "Wojtek"));

        Scanner scanner = new Scanner(System.in);
        String komenda;
        do {
            komenda = scanner.nextLine();

            String[] slowa = komenda.split(" ", 2);
            int waga = Integer.parseInt(slowa[0]);

            newsStation.powiadomOWiadomosci(slowa[1], waga);
        } while (!komenda.equalsIgnoreCase("quit"));
    }
}

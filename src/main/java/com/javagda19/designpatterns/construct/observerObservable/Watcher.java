package com.javagda19.designpatterns.construct.observerObservable;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Observable;
import java.util.Observer;

@Data
@AllArgsConstructor
public class Watcher implements Observer, ChangeListener<NewsMessage> {
    private int panicLevel;
    private String name;

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof NewsMessage) {
            NewsMessage newsMessage = (NewsMessage) arg;

            System.out.println("Ja " + name + " zostaje powiadomiony o :" +
                    newsMessage.getTresc());

            if (newsMessage.getWaga() > panicLevel) {
                System.out.println("Zaczynam panikowac, waga wiadomosci: " + newsMessage.getWaga());
            }
        }
    }

    @Override
    public void changed(ObservableValue<? extends NewsMessage> observable, NewsMessage oldValue, NewsMessage newValue) {
            
    }
}

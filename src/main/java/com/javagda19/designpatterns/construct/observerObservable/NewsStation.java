package com.javagda19.designpatterns.construct.observerObservable;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import lombok.Data;

import java.util.Observable;

@Data
public class NewsStation extends Observable {
    private String name;

    private SimpleObjectProperty<NewsMessage> newsMessageSimpleObjectProperty = new SimpleObjectProperty<>();

    public NewsStation(String name) {
        this.name = name;
    }

    public void dodajOgladajacego(Watcher w) {
        addObserver(w);
        newsMessageSimpleObjectProperty.addListener(w);
    }

    public void powiadomOWiadomosci(String tresc, int waga) {
        setChanged();
        notifyObservers(new NewsMessage(tresc, waga));

        newsMessageSimpleObjectProperty.setValue(new NewsMessage(tresc, waga));
    }
}
package com.javagda19.designpatterns.construct.decorator_christmastree;

public class Main {
    public static void main(String[] args) {
        ChristmasTree tree = new ChristmasTree(5);
        tree.printTreee();


        System.out.println();
        DecoratedChristmasTree decoratedChristmasTree = new DecoratedChristmasTree(tree, 3);
        decoratedChristmasTree.printTreee();
    }
}

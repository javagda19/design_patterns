package com.javagda19.designpatterns.construct.decorator_christmastree;

import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public class ChristmasTree implements ITree {
    private int size;

    @Override
    public void printTreee() {
        for (int i = 0; i < size; i++) {
            System.out.println(getTreeLine(i));
        }
    }

    public String getTreeLine(int lineNumber) {
        StringBuilder sb = new StringBuilder();
        for (int j = 0; j < (size - lineNumber - 1); j++) {
            sb.append(" ");
        }
        for (int j = 0; j < (lineNumber * 2 + 1); j++) {
            sb.append("*");
        }
        for (int j = 0; j < (size - lineNumber - 1); j++) {
            sb.append(" ");
        }
        return sb.toString();
    }
}

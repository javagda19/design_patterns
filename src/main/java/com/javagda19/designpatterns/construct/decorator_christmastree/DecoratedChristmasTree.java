package com.javagda19.designpatterns.construct.decorator_christmastree;

import lombok.AllArgsConstructor;

import java.util.Random;

@AllArgsConstructor
public class DecoratedChristmasTree implements ITree {
    private ChristmasTree christmasTree;
    private int decorationLevel;

    @Override
    public void printTreee() {
        for (int i = 0; i < christmasTree.getSize(); i++) {
            char[] treeLine = christmasTree.getTreeLine(i).toCharArray();

            for (int j = 0; j < treeLine.length; j++) {
                if (treeLine[j] == '*') {
                    int randomInt = new Random().nextInt(10); // 0 - 9
                    if (randomInt >= (10 - decorationLevel)) {
                        treeLine[j] = 'O';
                    }
                }
            }
            System.out.println(new String(treeLine));
        }
    }
}

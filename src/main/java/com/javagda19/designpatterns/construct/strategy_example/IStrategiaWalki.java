package com.javagda19.designpatterns.construct.strategy_example;

public interface IStrategiaWalki {
    void walcz();
}

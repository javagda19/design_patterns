package com.javagda19.designpatterns.construct.strategy_example;

public class StrategiaPodstawianiaOwcy implements IStrategiaWalki {
    public void walcz() {
        System.out.println("Podkładam smokowi owcę!");
    }
}

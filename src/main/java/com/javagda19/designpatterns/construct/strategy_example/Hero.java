package com.javagda19.designpatterns.construct.strategy_example;

public class Hero {
    private String name;
    private IStrategiaWalki strategia = new StrategiaWalkiMieczem();

    public Hero(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setStrategia(IStrategiaWalki strategia) {
        this.strategia = strategia;
    }

    public void walcz(){
        strategia.walcz();
    }
}

package com.javagda19.designpatterns.construct.strategy_example;

public class StrategiaWalkiMieczem implements IStrategiaWalki {
    public void walcz() {
        System.out.println("Walcze mieczem! Ciach Ciach!");
    }
}

package com.javagda19.designpatterns.construct.strategy_example;

import java.time.LocalDateTime;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        Hero h = new Hero("Marian");
//
//        Scanner scanner = new Scanner(System.in);
//
//        String komenda;
//        do {
//            System.out.println("Co chcesz zrobić?");
//            komenda = scanner.nextLine();
//
//            if (komenda.equalsIgnoreCase("zmien")) {
//                System.out.println("Podaj strategie [miecz, luk]");
//                komenda = scanner.nextLine();
//
//                if (komenda.equalsIgnoreCase("miecz")) {
//                    h.setStrategia(new StrategiaWalkiMieczem());
//
//                } else if (komenda.equalsIgnoreCase("luk")) {
//                    h.setStrategia(new StrategiaWalkiLukiem());
//
//                } else if (komenda.equalsIgnoreCase("owca")) {
//
//                    h.setStrategia(new StrategiaPodstawianiaOwcy());
//                }
//            } else if (komenda.equalsIgnoreCase("walcz")) {
//                System.out.println("Walcze!");
//                h.walcz();
//            }
//
//        } while (!komenda.equalsIgnoreCase("quit"));

        System.out.println(throwMeSomething(5, 0));
    }

    private static int throwMeSomething(int a, int b) {
        int c = -1;
        try {
            System.out.println("try");
            c = a / b;
        } catch (Exception e) {
            System.out.println("catch");
            return 0;
        } finally {
            System.out.println("finally");
        }
        System.out.println("tutaj");
        return c;
    }
}

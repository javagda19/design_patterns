package com.javagda19.designpatterns.construct.strategy_example;

public class StrategiaWalkiLukiem implements IStrategiaWalki {
    public void walcz() {
        System.out.println("Szczelam z łuku! Pew pew!");
    }
}

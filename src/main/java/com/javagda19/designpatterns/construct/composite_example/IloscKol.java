package com.javagda19.designpatterns.construct.composite_example;

public enum IloscKol {
    DWA,
    TRZY,
    CZTERY,
    PIEC,
    PIECDZIESIAT
}

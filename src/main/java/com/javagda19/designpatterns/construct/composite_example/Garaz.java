package com.javagda19.designpatterns.construct.composite_example;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Garaz {
    private List<IPojazd> pojazds = new ArrayList<>();

    public void dodajPojazd(IPojazd pojazd) {
        pojazds.add(pojazd);
    }

    public List<IPojazd> getPojazds() {
        return pojazds;
    }

    public List<Motocykl> getMotocykle() {
        return pojazds.stream()
                .filter(pojazd -> pojazd instanceof Motocykl)
                .map(pojazd -> (Motocykl) pojazd)
                .collect(Collectors.toList());
    }
}

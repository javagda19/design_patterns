package com.javagda19.designpatterns.construct.abstract_factory_example;

import com.javagda19.designpatterns.construct.abstract_factory_example.carpackage.Car;
import com.javagda19.designpatterns.construct.abstract_factory_example.carpackage.CarFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) {
//        Car carB = new Car("Audi", 1.61, 91, "Czerwony", 4);
        Car carB = CarFactory.createAudiA3();

//        Car carA = new Car("Audi", 1.61, 91, "Czerwony", 4);
        Car carA = CarFactory.createAudiA3();

        Car c = Car.createAudiA4();

//        Car carC = new Car("Audio", 1.41, 131, "Fioletowy", 13);
        ExecutorService executorService = Executors.newSingleThreadExecutor();
    }
}

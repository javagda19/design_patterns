package com.javagda19.designpatterns.construct.abstract_factory_example.carpackage;

public abstract class CarFactory {

    public static Car createAudiA3(){
        return new Car("Audi", 1.61, 90, "Czerwony", 4);
    }

//    public static Car createAudiA4(){
//        return new Car("Audi", 1.8, 130, "Zielony", 5);
//    }
}

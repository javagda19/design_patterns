package com.javagda19.designpatterns.construct.abstract_factory_example.carpackage;

public class Car {
    private String marka;
    private double pojemnoscSilnika;
    private int iloscKoniMech;
    private String kolor;
    private int iloscMiejsc;

    Car(String marka, double pojemnoscSilnika, int iloscKoniMech, String kolor, int iloscMiejsc) {
        this.marka = marka;
        this.pojemnoscSilnika = pojemnoscSilnika;
        this.iloscKoniMech = iloscKoniMech;
        this.kolor = kolor;
        this.iloscMiejsc = iloscMiejsc;
    }

    public static Car createAudiA4(){
        return new Car("Audi", 1.8, 130, "Zielony", 5);
    }

    public String getMarka() {
        return marka;
    }

    public void setMarka(String marka) {
        this.marka = marka;
    }

    public double getPojemnoscSilnika() {
        return pojemnoscSilnika;
    }

    public void setPojemnoscSilnika(double pojemnoscSilnika) {
        this.pojemnoscSilnika = pojemnoscSilnika;
    }

    public int getIloscKoniMech() {
        return iloscKoniMech;
    }

    public void setIloscKoniMech(int iloscKoniMech) {
        this.iloscKoniMech = iloscKoniMech;
    }

    public String getKolor() {
        return kolor;
    }

    public void setKolor(String kolor) {
        this.kolor = kolor;
    }

    public int getIloscMiejsc() {
        return iloscMiejsc;
    }

    public void setIloscMiejsc(int iloscMiejsc) {
        this.iloscMiejsc = iloscMiejsc;
    }

    @Override
    public String toString() {
        return "Car{" +
                "marka='" + marka + '\'' +
                ", pojemnoscSilnika=" + pojemnoscSilnika +
                ", iloscKoniMech=" + iloscKoniMech +
                ", kolor='" + kolor + '\'' +
                ", iloscMiejsc=" + iloscMiejsc +
                '}';
    }
}

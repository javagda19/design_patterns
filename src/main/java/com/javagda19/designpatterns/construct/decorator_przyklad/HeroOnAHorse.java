package com.javagda19.designpatterns.construct.decorator_przyklad;

public class HeroOnAHorse implements IHero {
    private int iloscZyciaKonia;
    private int dodatkowaSilaAtakuKonia;

    private Hero hero;

    public HeroOnAHorse(Hero hero) {
        this.hero = hero;
        this.iloscZyciaKonia = 100;
        this.dodatkowaSilaAtakuKonia = 100;
    }

    @Override
    public int getIloscZycia() {
        return hero.getIloscZycia() + iloscZyciaKonia;
    }

    @Override
    public int getSilaAtaku() {
        return hero.getSilaAtaku() + dodatkowaSilaAtakuKonia;
    }

    @Override
    public void zadajCios(int silaCiosu) {
        if (iloscZyciaKonia > silaCiosu) {
            iloscZyciaKonia -= silaCiosu;
        } else if (iloscZyciaKonia <= silaCiosu) {
            silaCiosu -= iloscZyciaKonia;
            System.out.println("Koń umiera.");
            iloscZyciaKonia = 0;

            hero.zadajCios(silaCiosu);
        }
    }
}

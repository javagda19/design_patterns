package com.javagda19.designpatterns.construct.decorator_przyklad;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
public class Hero implements IHero {
    private String imie;
    private int iloscZycia;
    private int silaAtaku;

    public int getIloscZycia() {
        return iloscZycia;
    }

    public int getSilaAtaku() {
        return silaAtaku;
    }

    @Override
    public void zadajCios(int silaCiosu) {
        iloscZycia -= silaCiosu;
    }
}

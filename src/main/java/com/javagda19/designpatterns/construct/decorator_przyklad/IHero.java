package com.javagda19.designpatterns.construct.decorator_przyklad;

public interface IHero {
    public int getIloscZycia();
    public int getSilaAtaku();
    public void zadajCios(int silaCiosu);
}
